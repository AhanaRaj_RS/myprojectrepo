package soln;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Stack;
 
public class ShortestRoute
{
    private int noOfNodes;
    private Stack<Integer> stack;
 
    public ShortestRoute()
    {
        stack = new Stack<Integer>();
    }
 
    public void distanceValidation(int distanceMatrix[][])
    {
        noOfNodes = distanceMatrix[1].length - 1;
        int[] visitedPoint = new int[noOfNodes + 1];
        visitedPoint[1] = 1;
        stack.push(1);
        int eachElement, distance = 0, i;
        int min = Integer.MAX_VALUE;
        boolean minFlag = false;
        System.out.print(1 + "\t");
 
        while (!stack.isEmpty())
        {
            eachElement = stack.peek();
            i = 1;
            min = Integer.MAX_VALUE;
            while (i <= noOfNodes)
            {
                if (distanceMatrix[eachElement][i] > 1 && visitedPoint[i] == 0)
                {
                    if (min > distanceMatrix[eachElement][i])
                    {
                        min = distanceMatrix[eachElement][i];
                        distance = i;
                        minFlag = true;
                    }
                }
                i++;
            }
            if (minFlag)
            {
                visitedPoint[distance] = 1;
                stack.push(distance);
                System.out.print(distance + "\t");
                minFlag = false;
                continue;
            }
            stack.pop();
        }
    }
 
    public static void main(String... arg)
    {
        int no_of_nodes;
        Scanner scanner = null;
        try
        {
            System.out.println("Enter the total number of nodes");
            scanner = new Scanner(System.in);
            no_of_nodes = scanner.nextInt();
            int distance_matrix[][] = new int[no_of_nodes + 1][no_of_nodes + 1];
            System.out.println("Provide the distance matrix");
            for (int i = 1; i <= no_of_nodes; i++)
            {
                for (int j = 1; j <= no_of_nodes; j++)
                {
                    distance_matrix[i][j] = scanner.nextInt();
                }
            }
            for (int i = 1; i <= no_of_nodes; i++)
            {
                for (int j = 1; j <= no_of_nodes; j++)
                {
                    if (distance_matrix[i][j] == 1 && distance_matrix[j][i] == 0)
                    {
                        distance_matrix[j][i] = 1;
                    }
                }
            }
            System.out.println("The shortest possible route after visiting each point is ");
            ShortestRoute shortestRoute = new ShortestRoute();
            shortestRoute.distanceValidation(distance_matrix);
        } catch (InputMismatchException inputMismatch)
         {
             System.out.println("Enter Input in correct format!!!");
         }
        scanner.close();
    }
}

